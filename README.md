### Challenge 2

#### Secure Runtime

**Explanations for the jury to have extra information about how you solved the task:**


Use a sandboxed container runtime to secure the application at runtime. The default containers are running without any depth protection. This approach comes to help us implement the defense-in-depth concept. Why we need this? There are a lot of vulnerabilities that can be exploited from external or internal access. Instead of using one protection from the application, we can add more defense into our container by container runtime like gVisor, Kata, etc., minimize attack surface from the attacker when he compromised our container and preventing them to affecting the host.

**Url you want to submit for the challenge:**


none

**Exact answer to the challenge:**


Kubernetes have a feature called **RuntimeClass**, we can use it to define our sandboxed container runtime by following content:

```
apiVersion: node.k8s.io/v1
kind: RuntimeClass
metadata:
  name: gvisor
handler: runsc
```

Before applying this YAML file, you need to check if your cluster already installed gVisor or Kata runtime so we can going forward to use it for our Pods. We created the cluster with 1 node (docker runtime), so we need to add more nodes that use containerd as their runtime because gVisor and Kata only works with containerd as mentioned by staff.
We're use SCW command-line to add more pool by the following command:

```
scw k8s cluster create project-id=cd1550a3-ee8c-4480-9470-5720f2c51fe5 region=nl-ams name=kbreak-ops-honk version=1.20.2 pools.0.name=default pools.0.autoscaling=true pools.0.autohealing=true pools.0.size=1 pools.0.min-size=1 pools.0.max-size=5 pools.0.node-type=DEV1-M tags.0=breakathon cni=cilium  ingress=traefik2
```

And we need to wait about 5 minutes or less until the nodes is ready.

Once done we can apply the previous YAML file to create **runtimeclass**.

```
kubectl apply -f gvisor.yaml
```

> **Note**: RuntimeClass will applied in the whole cluster namespace.

Before we deploying an application into workloads, we will add a label into new nodes because we have 1 node use **docker** as runtime so we can know which nodes can be used **runtimeclass** feature.

```
kubectl label nodes scw-xxxx runtime=containerd
```

Do the same thing to other nodes.

Then, we can use it into our Pods or Deployment manifests file. 

```
apiVersion: v1
kind: Pod
metadata:
  labels:
    run: nginx
  name: nginx
  namespace: ops-challenge2
spec:
  runtimeClassName: gvisor  // define your runtime here
  containers:
  - image: nginx
    name: nginx
  restartPolicy: Always
  nodeSelector:
    runtime: containerd     // specify label
```

Apply the above content using **kubectl**, the application will deploy in our workloads and the last step to verivy our container is running a sandboxed runtime or not as below.

```
kubectl exec -n ops-challenge2 nginx -- dmesg
```

The output should be like this.

```
[    0.000000] Starting gVisor...
[    0.453200] Segmenting fault lines...
[    0.473580] Forking spaghetti code...
[    0.724270] Adversarially training Redcode AI...
[    0.741776] Conjuring /dev/null black hole...
[    0.968976] Recruiting cron-ies...
[    1.382567] Verifying that no non-zero bytes made their way into /dev/zero...
[    1.501934] Mounting deweydecimalfs...
[    1.687183] Checking naughty and nice process list...
[    1.994792] Creating bureaucratic processes...
[    2.082682] Reticulating splines...
[    2.456239] Ready!
```

gVisor is there!

**Load Balancer services name:**


none


#### Enforce a Policy

**Explanations for the jury to have extra information about how you solved the task:**


In this case, we want to implement a Policy in the workloads to control our pods' creation at the cluster-level. We're using Kyverno to enforce the policy and follow security best practices for Kubernetes. Because in some case, maybe your container running and uses root as the default user, this is not recommended from a security perspective because it leads to full access to the attacker when compromised the container or maybe someone in your team creates a YAML file and enables some kernel capabilities to be loaded by pods, so that's why we need the Policy to enforce it.


**Url you want to submit for the challenge:**


none


**Exact answer to the challenge:**


We need to install Kyverno in our cluster.

```
kubectl create -f https://raw.githubusercontent.com/kyverno/kyverno/main/definitions/release/install.yaml
```

Then, install **kustomize** to help us applying any policies from Kyverno.

```
curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh"  | bash
```

Apply the policies.

```
kustomize build https://github.com/kyverno/policies/pod-security | kubectl apply -f -
```

The policies already follows [Kubernetes Pod Security Standards](https://kubernetes.io/docs/concepts/security/pod-security-standards/), so you can modify it as you needed to securing your Kubernetes environment.


**Load Balancer services name:**


none



we tried  to use different techniques
1- we updated all cluster2
2- we ran kube-bench node against the worker node and did not find major issues only 4 related to cert permissions
3-  we tried to use namespaces for isolation as much as possible
4- we installed policy engine Kyverno, and it caused us issues, so we put it in audit mode for  several policies and enforcing mode for one namespace where we use gvisor

5. we installed loki and grafan to be able to direct the audit logs and see status of the cluster, also would like to install  kyverno dashboard, but did not have time with the troubleshooting and dns validation issues
6- we used admission control as well kyverno replacement for PSP https://github.com/kyverno/policies/tree/main/pod-security
README for the exhausted gone to sleep member: https://gitlab.com/breakathon-honk/honk-ops-02/-/blob/master/README.md


we created Ingressroutes with Traefik2load balancer using external domain 
https://ops-secure.honk-ops.k8slab.org
http://ops2.honk-ops.k8slab.org
http://grafan.honk-ops.k8slab.org

started with admissin controls like always pull

